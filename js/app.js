$(function () {
    $.get("/units", function(data){        

    /* 1. Добавить уникальные классы в выпадающий слайд. 
       2. сделать так, чтобы при нажатии кнопки Save. Изменялись поля выбранного объекта. 
       3. Проверить, сбрасываются ли поля при нажатии Cancel. 
       4. 
       
       
       
       */

    var grades = _.uniq(_.pluck(data, 'grade'));
    var reformeUnit;
    var dialogTmpl = $("#dialog");
    $(".managerContent").buttonset();
    var dialog = new AddEditView(dialogTmpl, "#dialog-contaner");

    function openUnit(e) {
        var clickStud = $.trim($(this).text()); //допустим title уникальны. 
            
        var clickedObject = _.findWhere(data, { title: clickStud });
        clickedObject.grades = _.map(grades, function (item) { return { value: item, name: ("Grade " + item) } }); //{value : item, name : ("Grade" + item )});
        // var index = ?? индекс элемента
        var index = _.indexOf(grades, clickedObject.grade);
        clickedObject.grades[index].selected = true;

        dialog.render(clickedObject);
        //$("#dialog-contaner").append(dialog);

        return false;
    }


    var titleBlockTemple = $("#unit-item");
    var gradeTemple = $("#gradeItem"); 

    //_.uniq(_.pluck(data, 'grade'))
    titleBlockTemple.tmpl(data).appendTo("#selectable");
    // gradeTemple.tmpl(grades).appendTo("#gradeGroup");

    $("#selectable").selectable(); 
    //$('.studObjectText').css({"font-size":"150%"});
        
    $('.studObject').dblclick(openUnit);
        
        
    // $("#saveDialog").click(function (event) {
    //         alert("WW");
    //          var titleU= $("form [name='title']").val(); 
    //          var subjectU= $("form [name='subject']").val();  
    //          var gradeU= $("form [name='grade' selected= 'selected']").val();     
    //          var Unit= {title : titleU, subject : subjectU, grade : gradeU};  
    //             if(_.find(data, Unit)  ){
    //                 data.push(Unit);
    //             }     
    //         return false;
    //     
    // });
        
       
    
    $(document).on('click', '#saveDialog', function () {
        $('#selectable .studObject').remove();
        var titleU = $("form [name='title']").val();
        var subjectU = $("form [name='subject']").val();
        var gradeU = $("form [name='grade']").val();
        var Unit = { title: titleU, subject: subjectU, grade: +gradeU };
        if (!(_.contains(data, Unit))) {
            data.push(Unit);
            titleBlockTemple.tmpl(data).appendTo("#selectable");
            $("#selectable").selectable();
            $('.studObject').dblclick(openUnit);
        }
        dialog.dialog("close");
         
        //return false;
                         
    }.bind(this));


    $('#Add').click(function (event) {
        //как вызвать форму? 
        var AddForm = $("#dialog");
        var unit = {};
        unit.grades = _.map(grades, function (item) { return { value: item, name: ("Grade " + item) } });
        var addHtml = AddForm.tmpl(unit);
        dialog = addHtml.dialog();
        $("#dialog-contaner").append(dialog);

        return false;
    });

    $('#Delete').click(function (event) {
        var selectedUnit = $('#selectable  .ui-selected');
        var selectedTitle = $.trim($(selectedUnit).text());
        if (confirm("Вы уверены что хотите удалить юнит " + selectedTitle)) {
            data = _.reject(data, function (unitObj) { return unitObj.title === selectedTitle });  //?
            $('#selectable .studObject').remove();
            titleBlockTemple.tmpl(data).appendTo("#selectable");
            $("#selectable").selectable();
            $('.studObject').dblclick(openUnit);
        }
        return false;



    });


    $('#managerContent button').click(function (event) {
        // if(not-selected) ? selected : diselected;
                
    });
    var searchEngine = new Search("#selectable-container", data);

    $('#Search').click(function () {
        var MathematicksCheck = $('#Mathematics').is(":checked");
        var LiteratureCheck = $('#Literature').is(":checked");
        var GeographyCheck = $('#Geography').is(":checked");

searchEngine.searchBy({});

        var Grade1 = $('#Grade-1').is(":checked");
        var Grade2 = $('#Grade-2').is(":checked");
        var Grade3 = $('#Grade-3').is(":checked");
        var Grade4 = $('#Grade-4').is(":checked");
        var Grade5 = $('#Grade-5').is(":checked");
        //make flag selected button true. 
        function checkbockxFilter(unit) {
            var subject = unit.subject;
            switch (subject) {
                case 'Mathematics':
                    return MathematicksCheck;
                    break;
                case 'Literature':
                    return LiteratureCheck;
                    break;
                case 'Geography':
                    return GeographyCheck;
                    break;
                default:
                    alert('Error in checkConfirm');
            }
        }
        function gradeFilter(unit) {
            var grade = unit.grade;
            switch (grade) {
                case 1:
                    return Grade1;
                    break;
                case 2:
                    return Grade2;
                    break;
                case 3:
                    return Grade3;
                    break;
                case 4:
                    return Grade4;
                    break;
                case 5:
                    return Grade5;
                    break;
                default:
                    alert('Error in Grade Confirm');
            }
        }
                
        //filter outout
        var dataFilter = _.filter(data, function (unit) { return gradeFilter(unit) || checkbockxFilter(unit) });
        $('#selectable .studObject').remove();
        titleBlockTemple.tmpl(dataFilter).appendTo("#selectable");

        return false;
    });

    $('#Search-all').click(function () {
        $('#Mathematics').prop('checked', false);
        $('#Literature').prop('checked', false);
        $('#Geography').prop('checked', false);

        $('#Grade-1').attr('checked', false);
        $('#Grade-2').attr('checked', false);
        $('#Grade-3').attr('checked', false);
        $('#Grade-4').attr('checked', false);
        $('#Grade-5').attr('checked', false);
        //как отжать нажатую кнопку. 
                
        //make all selected dis(":checked");
        $('#selectable .studObject').remove();
        titleBlockTemple.tmpl(data).appendTo("#selectable");

        return false;
    });
            
    /*  $(this.title).appendTo("#titleReforme");
      $(this.subject).appendTo("#subjectReforme"); */

    });
});

