var http = require("http");
var fs = require('fs');
var port = 8080;
var easysearch = "easysearch.html";
var url = require("url");
var path = require("path");

http.createServer(function (request, response) {
    if(request.url === '/units') {
        fs.readFile("json.js", "binary", function(err, file){
            if (err) {
                response.writeHead(404, { "Content-Type": "text/plain" });
                response.write(err);
                response.end();
            } else {
                response.writeHead(200, { "Content-Type": "application/json" });
                response.write(file, "binary");
                response.end();
            }
        });
    } else {
        getContent(request, response);
    }
}).listen(port);

function getContent(request, response){
    var correctedUrl = (request.url === '/' || request.url === '') ? (request.url + easysearch) : request.url,
        uri = url.parse(correctedUrl).pathname,
        filename = path.join(process.cwd(), uri),
        extension = path.extname(filename),
        contentType = "";
    
    switch (extension) {
        case ".js":
            contentType = "text/javascript"
            break;    
        case ".css":
            contentType = "text/css"
            break;            
        case ".html":
            contentType = "text/html"
            break;
    }
        
    fs.readFile(filename, "binary", function(err, file) {        
            if (err) {
                response.writeHead(404, { "Content-Type": "text/plain" });
                response.write(err);
                response.end();
            } else {
                response.writeHead(200, { "Content-Type": contentType });
                response.write(file, "binary");
                response.end();
            }
        });
}